﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimManager : MonoBehaviour
{
    public Animator Personaje;

    public void Caminar()
    {
        Personaje.SetBool("Caminando", true);
        Personaje.SetBool("Arrastrando", false);
        Personaje.SetBool("Trepando", false);
        Personaje.SetBool("Tirando", false);
        Personaje.SetBool("Saltando", false);
    }
    public void Arrastrar()
    {
        Personaje.SetBool("Caminando", false);
        Personaje.SetBool("Arrastrando", true);
        Personaje.SetBool("Trepando", false);
        Personaje.SetBool("Tirando", false);
        Personaje.SetBool("Saltando", false);
    }
    public void Trepar()
    {
        Personaje.SetBool("Caminando", false);
        Personaje.SetBool("Arrastrando", false);
        Personaje.SetBool("Trepando", true);
        Personaje.SetBool("Tirando", false);
        Personaje.SetBool("Saltando", false);
    }
    public void Lanzar()
    {
        Personaje.SetBool("Caminando", false);
        Personaje.SetBool("Arrastrando", false);
        Personaje.SetBool("Trepando", false);
        Personaje.SetBool("Tirando", true);
        Personaje.SetBool("Saltando", false);
    }
    public void Saltar()
    {
        Personaje.SetBool("Caminando", false);
        Personaje.SetBool("Arrastrando", false);
        Personaje.SetBool("Trepando", false);
        Personaje.SetBool("Tirando", false);
        Personaje.SetBool("Saltando", true);
    }

}
